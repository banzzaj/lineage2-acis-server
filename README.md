# Lineage2 aCis Server

https://acis.i-live.eu/index.php?topic=1974.0

## Updating to the new release

```bash
svn checkout https://xp-dev.com/svn/aCis_public@7
git clone git@gitlab.com:banzzaj/lineage2-acis-server.git
rm -rf lineage2-acis-server/aCis_*
cp -R aCis_public/aCis_* lineage2-acis-server
cd lineage2-acis-server
git add . && git push origin HEAD
```
